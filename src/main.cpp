// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "diag/Trace.h"
#include "stm32f10x.h"
#include "stm32f10x_spi.h"

#include "OmniLock.h"
#include "MFRC522.h"
#include "Ticks.h"

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

uint8_t spiWrite(uint8_t data)
{
	while (!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));
	SPI_I2S_SendData(SPI1, data);
	while (!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE));
	return (uint8_t) SPI_I2S_ReceiveData(SPI1);
}

int main(int argc, char* argv[])
{
	Ticks::Init();

	// ---------------------
	// Configure GPIO
	// ---------------------
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

	GPIO_InitTypeDef GPIO_InitStruct;

	// LED
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);

	// MCO (alternate function for PA8)
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	RCC_MCOConfig(RCC_MCO_SYSCLK);

	// ---------------------
	// Configure SPI
	// ---------------------
	// SPI pins: PA5 (SCK), PA6 (MISO), PA7 (MOSI)
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	// PA4 (NSS): software controlled
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	GPIO_SetBits(GPIOA, GPIO_Pin_4);

	// SPI
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	SPI_InitTypeDef SPI_InitStruct;
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;
	SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_Init(SPI1, &SPI_InitStruct);
	SPI_SSOutputCmd(SPI1, ENABLE);
	SPI_Cmd(SPI1, ENABLE);

	MFRC522 rfid(SPI1, GPIOA, GPIO_Pin_4);
	rfid.PCD_Init();

	uint8_t uidByteOn[] = {0xE5, 0x95, 0x15};
	uint8_t uidByteOff[] = {0x5D, 0x0C, 0x88};
	MFRC522::MIFARE_Key key = {{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}};

	int32_t nextHeartbeat = 0;

	OmniLock::HardwareConfig hw;
	hw.GpioMotorA = GPIOA;
	hw.PinMotorA = GPIO_Pin_2;
	hw.GpioMotorB = GPIOA;
	hw.PinMotorB = GPIO_Pin_3;
	hw.GpioSwitchStep = GPIOA;
	hw.PinSwitchStep = GPIO_Pin_0;
	hw.GpioSwitchClosed = GPIOA;
	hw.PinSwitchClosed = GPIO_Pin_1;
	OmniLock omni(hw);

	while (1) {
		omni.MoveOneStep();
		Ticks::DelayMs(1000);

//		if (!rfid.PICC_IsNewCardPresent()) continue;
//		if (!rfid.PICC_ReadCardSerial()) continue;
//		MFRC522::Uid uid = rfid.uid;
//		MFRC522::StatusCode status;
//		status = rfid.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 7, &key, &uid);
//		if (status == MFRC522::STATUS_OK) {
//			if (memcmp(uidByteOn, uid.uidByte, 3) == 0) {
//				GPIO_SetBits(GPIOB, GPIO_Pin_12);
//			} else if (memcmp(uidByteOff, uid.uidByte, 3) == 0) {
//				GPIO_ResetBits(GPIOB, GPIO_Pin_12);
//			}
//			rfid.PCD_StopCrypto1();
//		}
//		if (Ticks::HasElapsed(nextHeartbeat)) {
//			nextHeartbeat = Ticks::Get() + SystemCoreClock;
//			GPIO_WriteBit(GPIOB, GPIO_Pin_12,
//					(BitAction) !GPIO_ReadOutputDataBit(GPIOB, GPIO_Pin_12));
//		}
	}
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
